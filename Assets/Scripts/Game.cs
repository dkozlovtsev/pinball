﻿using System;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Game : MonoBehaviour
{
    public SelectModeScreen modeScreen;
    public SelectLevelScreen levelScreen;
    public PointsDisplay pointsDisplay;
    public UiController mobileUiController;
    public UiController pcUiController;
    public FinishScreenShower finishScreenShower;


    public void Start()
    {
        GameModel model = null;
        if (Application.platform == RuntimePlatform.WindowsPlayer || 
            Application.platform == RuntimePlatform.WindowsEditor ||
            Application.platform == RuntimePlatform.OSXEditor ||
            Application.platform == RuntimePlatform.OSXPlayer ||
            Application.platform == RuntimePlatform.LinuxPlayer
            )
        {
            model = new GameModel(modeScreen, levelScreen, pointsDisplay, pcUiController, finishScreenShower);
        }
        else
        {
            model = new GameModel(modeScreen, levelScreen, pointsDisplay, mobileUiController, finishScreenShower);
            
        }
        this.StartCoroutine(model.Main());
    }
}
