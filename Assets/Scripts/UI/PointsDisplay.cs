﻿using UnityEngine;
using UnityEngine.UI;

public class PointsDisplay : MonoBehaviour, IPointsDisplay
{
    public Text text;
    private float _points;

    public float points
    {
        get { return _points; }
        set
        {
            _points = value;
            text.text = _points.ToString();
        }
    }
}