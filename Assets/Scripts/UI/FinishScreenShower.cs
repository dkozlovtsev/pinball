﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FinishScreenShower : MonoBehaviour, IFinishScreenShower
{
    public Button retry;
    public Button finish;

    private bool _typeSelected = false;
    private ResultHolder<FinishTypes> _finishType;
    private void OnRetryClicked()
    {
        _typeSelected = true;
        _finishType.result = FinishTypes.Retry;

    }

    private void OnFinishClicked()
    {
        _typeSelected = true;
        _finishType.result = FinishTypes.Restart;
    }

    public IEnumerator ShowFinishScreen(ResultHolder<FinishTypes> result)
    {
        this.gameObject.SetActive(true);

        if (_finishType != null)
            throw new Exception("Only one show at a time");

        _finishType = result;
        retry.onClick.AddListener(OnRetryClicked);
        finish.onClick.AddListener(OnFinishClicked);

        while (_typeSelected == false)
        {
            yield return null;
        }

        
        _finishType = null;
        _typeSelected = false;

        retry.onClick.RemoveListener(OnRetryClicked);
        finish.onClick.RemoveListener(OnFinishClicked);
        this.gameObject.SetActive(false);
    }
}