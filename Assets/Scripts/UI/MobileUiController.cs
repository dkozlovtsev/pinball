﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;


public class MobileUiController : UiController
{
    public float maxDisplacement = 200;
    public EventTrigger touchEventTrigger;
    public EventTrigger leftEventTrigger;
    public EventTrigger rightEventTrigger;

    private class Acceleration : IAcceleration
    {
        private readonly MobileUiController _parent;


        public Acceleration(MobileUiController parent)
        {
            _parent = parent;
            parent.touchEventTrigger.Subscribe(EventTriggerType.EndDrag, OnDragEnd);
            parent.touchEventTrigger.Subscribe(EventTriggerType.Drag, OnDrag);
            parent.touchEventTrigger.Subscribe(EventTriggerType.BeginDrag, OnDragStart);
        }

        public void Dispose()
        {
            _parent.touchEventTrigger.triggers.Clear();

            accelerationStarted = null;
            accelerationChanged = null;
            accelerationEnded = null;
        }

        public event Action accelerationStarted = delegate {  };
        public event Action<float> accelerationChanged = delegate { };
        public event Action accelerationEnded = delegate { };

        private Vector2 _dragStartPosition = Vector2.zero;
        private void OnDragStart(BaseEventData data)
        {
            _dragStartPosition = ((PointerEventData) data).position;
            accelerationStarted();
        }

        private void OnDragEnd(BaseEventData data)
        {
            accelerationEnded();
        }

        private void OnDrag(BaseEventData data)
        {
            accelerationChanged(Mathf.Clamp01((_dragStartPosition - ((PointerEventData) data).position).y/_parent.maxDisplacement));
        }
    }

    public override IAcceleration StartAcceleration()
    {
        return new Acceleration(this);
    }    

    public override void Dispose()
    {
        base.ClearSubscribers();
        leftEventTrigger.triggers.Clear();
        rightEventTrigger.triggers.Clear();
        touchEventTrigger.triggers.Clear();

        _beingShown = false;
        this.gameObject.SetActive(false);
    }


    private bool _beingShown = false;
    public override IUIController Show()
    {
        if (_beingShown)
            throw new Exception("Only one show at a time");

        _beingShown = true;
        this.gameObject.SetActive(true);
        leftEventTrigger.Subscribe(EventTriggerType.PointerDown, (data) => base.InvokeLeftFlipper(true));
        leftEventTrigger.Subscribe(EventTriggerType.PointerUp, (data) => base.InvokeLeftFlipper(false));
        rightEventTrigger.Subscribe(EventTriggerType.PointerDown, (data) => base.InvokeRightFlipper(true));
        rightEventTrigger.Subscribe(EventTriggerType.PointerUp, (data) => base.InvokeRightFlipper(false));
        return this;
    }
}