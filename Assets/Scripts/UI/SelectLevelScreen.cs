﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelScreen : MonoBehaviour, IFieldSelector
{

    public Transform parent;
    public Button prototype;
    public Levels levels;

    private List<Button> _buttons = new List<Button>();
    private Button AddButton(string text)
    {

        Button button = GameObject.Instantiate(prototype);
        
        button.name = text;
        button.transform.SetParent(parent, false);
        button.gameObject.SetActive(true);



        var label = button.transform.GetComponentInChildren<Text>();
        label.text = text;

        _buttons.Add(button);
        return button;
    }

    private void Clear()
    {
        foreach (var button in _buttons)
        {
            GameObject.Destroy(button.gameObject);
        }
        _buttons.Clear();
    }

    private class FieldShower: IFieldShower
    {
        
        private readonly Field _field;

        public FieldShower(Field field)
        {
            _field = field;
        }

        public IDisposableField Show()
        {
            var go = GameObject.Instantiate(_field);
            go.gameObject.SetActive(true);
            return new DisposableField(go);
            
        }
    }

    private class DisposableField: IDisposableField
    {
        private Field _field;
        public DisposableField(Field field)
        {
            _field = field;
        }
        
        public string fieldName
        {
            get { return _field.fieldName; }
        }

        public IFlipper leftFlipper
        {
            get { return _field.leftFlipper; }
        }

        public IFlipper rightFlipper
        {
            get { return _field.rightFlipper; }
        }

        public IHitter hitter
        {
            get { return _field.hitter; }
        }

        public event Action ballLeftGameField
        {
            add { _field.ballLeftGameField += value; }
            remove { _field.ballLeftGameField -= value; }
        }

        public IAIHelper leftAiHelper
        {
            get { return _field.leftAiHelper; }
        }

        public IAIHelper rightAiHelper
        {
            get { return _field.rightAiHelper; }
        }

        public void Dispose()
        {
            GameObject.Destroy(_field.gameObject);
        }
    }


    private Field _tmpField;

    private class Workaround
    {
        private readonly ResultHolder<Field> _field;
        private readonly Field _valueToSet;

        public Workaround(ResultHolder<Field> field, Field valueToSet)
        {
            _field = field;
            _valueToSet = valueToSet;
        }

        public void Listen()
        {
            _field.result = _valueToSet;
        }
    }

    public IEnumerator Select(ResultHolder<IFieldShower> fieldShower)
    {
        this.gameObject.SetActive(true);

        //Field selectedField = null;
        //for (int i = 0; i < levels.fields.Count; i++)
        //{
        //    var field = levels.fields[i];
        //    var button = AddButton(field.fieldName);
        //    button.onClick.AddListener(() => { Debug.Log(y);});
        //}
        //The code above doesn't seem to work, ?generator confuses compiler?


        ResultHolder<Field> selectedField = new ResultHolder<Field>();
        for (int i = 0; i < levels.fields.Count; i++)
        {
            var field = levels.fields[i];
            var button = AddButton(field.fieldName);
            button.onClick.AddListener(new Workaround(selectedField, field).Listen);
        }


        while (selectedField.result == null)
        {
            yield return null;
        }

        fieldShower.result = new FieldShower(selectedField.result);

        Clear();
        this.gameObject.SetActive(false);
    }

}