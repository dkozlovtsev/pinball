﻿using System;
using UnityEngine;

public abstract class UiController : MonoBehaviour, IUIController, IUIControllerShower
{
    public abstract void Dispose();

    public abstract IAcceleration StartAcceleration();
    public event Action<bool> leftFlipperActive = delegate { };
    public event Action<bool> rightFlipperActive = delegate { };

    protected void InvokeLeftFlipper(bool value)
    {
        leftFlipperActive(value);
    }
    protected void InvokeRightFlipper(bool value)
    {
        rightFlipperActive(value);
    }

    protected void ClearSubscribers()
    {
        leftFlipperActive = delegate { };
        rightFlipperActive = delegate { };
    }

    public abstract IUIController Show();
}