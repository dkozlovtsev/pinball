﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectModeScreen : MonoBehaviour, IModeSelector
{
    public Button aiMode;
    public Button uiMode;

    public IEnumerator Show(ResultHolder<Modes> modes)
    {
        this.gameObject.SetActive(true);

        if (_modes != null)
            throw new Exception("Only one show at a time");

        _modes = modes;
        aiMode.onClick.AddListener(OnAiModeClicked);
        uiMode.onClick.AddListener(OnUiModeClicked);

        while (_modeSelected == false)
        {
            yield return null;
        }

        _modes = null;
        _modeSelected = false;
        aiMode.onClick.RemoveListener(OnAiModeClicked);
        uiMode.onClick.RemoveListener(OnUiModeClicked);
        this.gameObject.SetActive(false);
    }

    private bool _modeSelected = false;
    private ResultHolder<Modes> _modes;
    private void OnAiModeClicked()
    {
        _modeSelected = true;
        _modes.result = Modes.AI;
        
    }

    private void OnUiModeClicked()
    {
        _modeSelected = true;
        _modes.result = Modes.UI;

    }
}