﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PCUiController : UiController
{

    public KeyCode rightFlipperKey;
    public KeyCode leftFlipperKey;
    public KeyCode accelerationKey;

    public Text helpText;

    public float accelerationIncreaseSpeed = 0.5f;


    public void Start()
    {
        helpText.text = string.Format("Use {0} and {1} to control flippers, press {2} and release to start",
            rightFlipperKey, leftFlipperKey, accelerationKey);
    }

    private class Acceleration: IAcceleration
    {
        private readonly PCUiController _parent;
        private float _accelerationBackingField;

        public Acceleration(PCUiController parent)
        {
            _parent = parent;
        }

        private float acceleration
        {
            get { return _accelerationBackingField; }
            set
            {
                _accelerationBackingField = value;
                accelerationChanged(value);
            }
        }

        public IEnumerator StartRoutine()
        {
            while (!Input.GetKeyDown(_parent.accelerationKey))
            {
                yield return null;
            }

            accelerationStarted();
            do
            {
                acceleration += _parent.accelerationIncreaseSpeed*Time.deltaTime;
                yield return null;
            } while (!Input.GetKeyUp(_parent.accelerationKey));

            accelerationEnded();
        }

        public event Action accelerationStarted = delegate {  };
        public event Action<float> accelerationChanged = delegate { };
        public event Action accelerationEnded = delegate { };

        public void Dispose()
        {
            accelerationStarted = delegate { };
            accelerationChanged = delegate { };
            accelerationEnded = delegate { };
        }
    }


    public override void Dispose()
    {
        base.ClearSubscribers();
        _beingShown = false;
        this.gameObject.SetActive(false);
    }

    
    public override IAcceleration StartAcceleration()
    {
        var acceleration = new Acceleration(this);
        this.StartCoroutine(acceleration.StartRoutine());
        return acceleration;
    }

    private bool _beingShown = false;
    private bool _leftFlipperState;
    private bool _rightFlipperState;

    public override IUIController Show()
    {
        if (_beingShown)
            throw new Exception("Only one show at a time");

        _beingShown = true;
        this.gameObject.SetActive(true);
        return this;
    }

    private bool leftFlipperState
    {
        get { return _leftFlipperState; }
        set
        {
            if (_leftFlipperState != value)
            {
                _leftFlipperState = value;
                base.InvokeLeftFlipper(value);
            }
        }
    }

    private bool rightFlipperState
    {
        get { return _rightFlipperState; }
        set
        {
            if (_rightFlipperState != value)
            {
                _rightFlipperState = value;
                base.InvokeRightFlipper(value);
            }
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(leftFlipperKey))
            leftFlipperState = true;
        else if (Input.GetKeyUp(leftFlipperKey))
            leftFlipperState = false;

        if (Input.GetKeyDown(rightFlipperKey))
            rightFlipperState = true;
        else if (Input.GetKeyUp(rightFlipperKey))
            rightFlipperState = false;
    }
}