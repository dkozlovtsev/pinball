﻿using System;
using UnityEngine;

public class ActiveZone : MonoBehaviour
{
    public event Action somethingExited = delegate { };

    void OnTriggerExit(Collider collider)
    {
        somethingExited();
    }
}