﻿using System;
using UnityEngine;
using System.Collections;


public class Hitter : MonoBehaviour, IHitter, IHitProcess
{
    public GameObject ballPrefab;

    public Transform spawnPoint;
    public Transform hitterTransform;

    public Vector3 maxDisplacement = new Vector3(0, 4.0f, 0);

    /// <summary>
    /// Max Starting impulse in local coordinates
    /// </summary>
    public Vector3 maxStartingImpulse = new Vector3(0, 10.0f, 0);
    

    private bool _isLaunching = false;
    private Vector3 _startPosition;
    private float _strenght;


    private void Start()
    {
        _startPosition = hitterTransform.localPosition;
    }

    private GameObject _ballInstance;
    private Rigidbody _ballRigidbody;
    private Transform _ballTransform;
    public IHitProcess Launch()
    {
        if (_isLaunching)
            throw new Exception("Launch already started");

        _strenght = 0;
        _isLaunching = true;


        
        _ballInstance = GameObject.Instantiate(ballPrefab);
        _ballTransform = _ballInstance.transform;
        _ballTransform.SetParent(this.transform);
        _ballTransform.position = spawnPoint.position;
        _ballRigidbody = _ballInstance.GetComponent<Rigidbody>();
        _ballRigidbody.isKinematic = true;
            
        hitterTransform.localPosition = _startPosition;
        return this;
    }

    public float strenght
    {
        get { return _strenght; }
        set
        {
            if (!_isLaunching)
                throw new Exception("Launch has not been started");

            _strenght = Mathf.Clamp01(value);
            _ballTransform.position = spawnPoint.position;
            hitterTransform.localPosition = _startPosition + maxDisplacement*value;
        }
    }

    public IBall FinishLaunch()
    {
        if (!_isLaunching)
            throw new Exception("Launch has not been started");

        _isLaunching = false;
        
        hitterTransform.localPosition = _startPosition;
        _ballTransform.position = spawnPoint.position;
        
        _ballRigidbody.isKinematic = false;
        _ballRigidbody.AddForce(hitterTransform.TransformDirection(maxStartingImpulse * strenght), ForceMode.Impulse);

        return _ballRigidbody.gameObject.GetComponent<Ball>();
    }


    public bool start;

    void Update()
    {
        if (start)
        {
            this.StartCoroutine(TestRoutine());
            start = false;
        }
    }

    IEnumerator TestRoutine()
    {
        yield return null;
        var process = this.Launch();
        float step = 1.0f/100;
        for (int i = 0; i < 100; i++)
        {
            process.strenght += step;
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1.1f);
        process.FinishLaunch();
    }

}
