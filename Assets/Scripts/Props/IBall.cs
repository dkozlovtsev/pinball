﻿using System;
using System.Collections.Generic;


public interface IBallHitter
{
    //as we only need points from it
    float points { get; }
}

public delegate void BallHitInfo(IBallHitter hitter);


public interface IBall: IDisposable
{
    event BallHitInfo gotHit;
}

public interface IHittableBall
{
    void Hit(IBallHitter info);
}