using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour, IBall, IHittableBall
{
    public event BallHitInfo gotHit = delegate { };

    public void Hit(IBallHitter hitter)
    {
        gotHit(hitter);
    }

    public void Dispose()
    {
        GameObject.Destroy(this.gameObject);
    }
}