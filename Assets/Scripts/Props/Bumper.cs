﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider))]
public class Bumper : MonoBehaviour , IBallHitter
{

    [Tooltip("Absolute value of impulse that affects ball after collision( > 0)")]
    public float impulseMagnitude;

    [Tooltip("Absolute value of random variation for the impulse strenght")]
    public float impulseVariation;


    private Collider _collider;

    public void Start()
    {
        _collider = this.GetComponent<Collider>();
    }

    void OnCollisionEnter(Collision collision)
    {
        IHittableBall ball = collision.rigidbody.gameObject.GetComponent<IHittableBall>();
        if (ball != null)
        {
            float variation = UnityEngine.Random.Range(-1.0f, 1.0f);

            var point = collision.contacts[0].point;

            var dir = collision.contacts[0].normal;
            point -= dir;
            RaycastHit hitInfo;

            if (_collider.Raycast(new Ray(point, dir), out hitInfo, 4))
            {
                collision.collider.attachedRigidbody.AddForce(
                     hitInfo.normal * (impulseMagnitude + variation * impulseVariation), ForceMode.Impulse);

                //Debug.DrawRay(collision.contacts[0].point, hitInfo.normal, Color.green, 10);
                //UnityEditor.EditorApplication.isPaused = true;
            }
            collided.Invoke();
            ball.Hit(this);
        }
    }

    public UnityEvent collided = new UnityEvent();

    public float pointsToGive;
    float IBallHitter.points { get { return pointsToGive; } }
}
 