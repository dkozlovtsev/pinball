﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Field : MonoBehaviour, IField
{
    public string fieldName;

    public Flipper leftFlipper;
    public Flipper rightFlipper;
    public Hitter hitter;

    public AiHelperBase leftAIHelperObject;
    public AiHelperBase rightAIHelperObject;

    public ActiveZone gameZone;

    string IField.fieldName { get { return fieldName; }}
    IFlipper IField.leftFlipper { get { return leftFlipper; } }
    IFlipper IField.rightFlipper { get { return rightFlipper; } }
    IHitter IField.hitter { get { return hitter; } }
    public event Action ballLeftGameField = delegate {  };

    public IAIHelper leftAiHelper
    {
        get
        {
            return leftAIHelperObject;
        
        }
    }

    public IAIHelper rightAiHelper
    {
        get
        {
            return rightAIHelperObject;
        }
    }

    public void Start()
    {
        gameZone.somethingExited += OnBallExited;
    }

    private void OnBallExited()
    {
        ballLeftGameField();
    }
}
