﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Accelerator : MonoBehaviour
{
    public Vector3 relativeForce;

    public Vector3 forceToApply;

    void OnTriggerStay(Collider collider)
    {

        forceToApply = this.transform.TransformDirection(relativeForce);

        if (collider.attachedRigidbody != null)
        {
            collider.attachedRigidbody.AddForce(forceToApply);
            //collider.attachedRigidbody.AddRelativeForce(relativeForce);
        }
    }

}
