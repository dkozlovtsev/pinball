﻿using UnityEngine;
using System.Collections;


public class Flipper : MonoBehaviour, IFlipper
{

    public HingeJoint joint;
    private bool _active;


    public bool active
    {
        get { return _active; }
        set
        {
            if (_active != value)
            {
                JointMotor motor = joint.motor;
                motor.targetVelocity = -motor.targetVelocity;
                joint.motor = motor;

                _active = value;
            }
        }
    }
}
