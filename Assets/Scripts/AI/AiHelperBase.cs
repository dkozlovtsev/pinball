﻿using System;
using System.Collections.Generic;
using UnityEngine;

public abstract class AiHelperBase : MonoBehaviour, IAIHelper
{
    public event Action<bool> flipperState = delegate { };

    protected void InvokeflipperState(bool state)
    {
        flipperState(state);
    }
}