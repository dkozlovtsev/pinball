﻿using System.Collections.Generic;

public class CompoundAiHelper : AiHelperBase
{
    public List<AiHelperBase> helpers;

    public void Start()
    {
        foreach (var aiHelperBase in helpers)
        {
            aiHelperBase.flipperState += OnAiHelperBaseOnFlipperState;
        }
    }

    private void OnAiHelperBaseOnFlipperState(bool b)
    {
        base.InvokeflipperState(b);
    }
}