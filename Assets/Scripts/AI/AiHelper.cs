﻿using System.Collections.Generic;
using UnityEngine;

public class AiHelper : AiHelperBase
{
    public bool active;

    private Collider _cacheCollider;
    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.GetComponent<IBall>() != null)
        {
            base.InvokeflipperState(active);
        }
    }

}