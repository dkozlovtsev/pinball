﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEditor;

public static class  EditorUtility
{
    
    public static void CreateAsset<T> () where T : ScriptableObject
	{
		T asset = ScriptableObject.CreateInstance<T> ();
 
		string path = AssetDatabase.GetAssetPath (Selection.activeObject);
		if (path == "") 
		{
			path = "Assets";
		} 
		else if (Path.GetExtension (path) != "") 
		{
			path = path.Replace (Path.GetFileName (AssetDatabase.GetAssetPath (Selection.activeObject)), "");
		}
 
		string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath (path + "/New " + typeof(T).ToString() + ".asset");
 
		AssetDatabase.CreateAsset (asset, assetPathAndName);
 
		AssetDatabase.SaveAssets ();
        	AssetDatabase.Refresh();
		UnityEditor.EditorUtility.FocusProjectWindow ();
		Selection.activeObject = asset;
	}


    [MenuItem("Create/Pinball/Levels")]
    public static void CreateLevels()
    {
        CreateAsset<Levels>();
    }


}
