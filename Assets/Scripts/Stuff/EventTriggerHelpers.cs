﻿using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public static class EventTriggerHelpers
{
    public static void Subscribe(this EventTrigger trigger, EventTriggerType eventType, UnityAction<BaseEventData> cb)
    {
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = eventType;
        entry.callback.AddListener(cb);
        if (trigger.triggers == null)
            trigger.triggers = new List<EventTrigger.Entry>();
        trigger.triggers.Add(entry);
    }
}