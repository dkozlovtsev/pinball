﻿using System;

public interface IUIController: IDisposable
{
    IAcceleration StartAcceleration();

    event Action<bool> leftFlipperActive;
    event Action<bool> rightFlipperActive;
}