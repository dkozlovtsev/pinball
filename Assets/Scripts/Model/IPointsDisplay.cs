﻿public interface IPointsDisplay
{
    float points { get; set; }
}