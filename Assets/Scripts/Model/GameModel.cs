﻿using System;
using System.Collections;
using UnityEngine;

public class GameModel
{
    private readonly IModeSelector _modeSelector;
    private readonly IFieldSelector _fieldSelector;
    private readonly IPointsDisplay _pointsDisplay;
    private readonly IUIControllerShower _uiControllerShower;
    private readonly IFinishScreenShower _finishScreenShower;

    public GameModel(IModeSelector modeSelector, IFieldSelector fieldSelector, IPointsDisplay pointsDisplay,
        IUIControllerShower uiControllerShower, IFinishScreenShower finishScreenShower)
    {
        _modeSelector = modeSelector;
        _fieldSelector = fieldSelector;
        _pointsDisplay = pointsDisplay;
        _uiControllerShower = uiControllerShower;
        _finishScreenShower = finishScreenShower;
    }

    public IEnumerator Main()
    {
        do
        {
            _pointsDisplay.points = 0;
            ResultHolder<Modes> modeHolder = new ResultHolder<Modes>();
            yield return _modeSelector.Show(modeHolder);
            var mode = modeHolder.result;

            ResultHolder<IFieldShower> fieldHolder = new ResultHolder<IFieldShower>();
            yield return _fieldSelector.Select(fieldHolder);
            var fieldShower = fieldHolder.result;

            using (var field = fieldShower.Show())
            {
                if (mode == Modes.UI)
                {
                    yield return UIRoutine(field);
                }
                else
                {
                    yield return AIRoutine(field);
                }
            }
        } while (true);

    }

    private IEnumerator UIRoutine(IField field)
    {
        bool retry = false;
        do
        {
            retry = false;
            using (var uiController = _uiControllerShower.Show())
            {
                uiController.leftFlipperActive += (state) => field.leftFlipper.active = state;
                uiController.rightFlipperActive += (state) => field.rightFlipper.active = state;
                var process = field.hitter.Launch();
                using (var drag = uiController.StartAcceleration())
                {
                    bool dragStarted = false;
                    drag.accelerationStarted += () => dragStarted = true;

                    float dragValue = 0.0f;
                    drag.accelerationChanged += (newValue) => dragValue = newValue;

                    bool dragEnded = false;
                    drag.accelerationEnded += () => dragEnded = true;

                    while (!dragStarted)
                        yield return null;

                    do
                    {
                        process.strenght = Mathf.Clamp01(dragValue);
                        yield return null;
                    } while (!dragEnded);

                }

                var ball = process.FinishLaunch();

                using (ball)
                {
                    ball.gotHit += (hitInfo) => _pointsDisplay.points += hitInfo.points;

                    bool ballLeft = false;
                    Action ballLeftAction = () => ballLeft = true;
                    field.ballLeftGameField += ballLeftAction;
                    try
                    {
                        while (!ballLeft)
                            yield return null;
                    }
                    finally
                    {
                        field.ballLeftGameField -= ballLeftAction;
                    }
                }

                ResultHolder<FinishTypes> finishHolder = new ResultHolder<FinishTypes>();
                yield return _finishScreenShower.ShowFinishScreen(finishHolder);
                var finish = finishHolder.result;
                retry = finish == FinishTypes.Retry;
            }

          
        } while (retry);

    }

    private IEnumerator AIRoutine(IField field)
    {
        bool retry = false;
        do
        {
            Action<bool> leftAction = (state) => field.leftFlipper.active = state;
            field.leftAiHelper.flipperState += leftAction;
            Action<bool> rightAction = (state) => field.rightFlipper.active = state;
            field.rightAiHelper.flipperState += rightAction;

            try
            {
                var process = field.hitter.Launch();
                var strength = UnityEngine.Random.value;
                float speed = 0.5f;

                while (process.strenght < strength)
                {
                    var newStrength = process.strenght + speed * Time.deltaTime;
                    if (newStrength > strength)
                    {
                        process.strenght = strength;
                    }
                    else
                    {
                        process.strenght = newStrength;
                    }
                    yield return null;
                }

                using (var ball = process.FinishLaunch())
                {
                    ball.gotHit += (hitInfo) => _pointsDisplay.points += hitInfo.points;

                    bool ballLeft = false;
                    Action ballLeftAction = () => ballLeft = true;
                    field.ballLeftGameField += ballLeftAction;
                    try
                    {
                        while (!ballLeft)
                            yield return null;
                    }
                    finally
                    {
                        field.ballLeftGameField -= ballLeftAction;
                    }
                }

                ResultHolder<FinishTypes> finishHolder = new ResultHolder<FinishTypes>();
                yield return _finishScreenShower.ShowFinishScreen(finishHolder);
                var finish = finishHolder.result;
                retry = finish == FinishTypes.Retry;
            }
            finally
            {
                field.leftAiHelper.flipperState -= leftAction;
                field.rightAiHelper.flipperState -= rightAction;
            }
        } while (retry);
    }
}