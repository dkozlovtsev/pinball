﻿using System;

public interface IDisposableField : IField, IDisposable
{
    
}