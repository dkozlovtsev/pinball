﻿using System.Collections;

public interface IFieldSelector
{
    IEnumerator Select(ResultHolder<IFieldShower> field);
}