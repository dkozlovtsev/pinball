﻿using System;
using System.Collections.Generic;


public interface IAIHelper
{
    event Action<bool> flipperState;
}

public interface IField 
{
    string fieldName { get; }

    IFlipper leftFlipper
    {
        get;
    }

    IFlipper rightFlipper { get; }

    IHitter hitter { get; }

    event Action ballLeftGameField;

    IAIHelper leftAiHelper { get; }
    IAIHelper rightAiHelper { get; }
}