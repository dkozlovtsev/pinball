﻿using System.Collections.Generic;

public interface IHitProcess
{
    float strenght { get; set; }
    IBall FinishLaunch();
}