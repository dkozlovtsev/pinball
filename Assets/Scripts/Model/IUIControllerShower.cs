﻿public interface IUIControllerShower
{
    IUIController Show();
}