﻿using System.Collections;

public interface IModeSelector
{
    IEnumerator Show(ResultHolder<Modes> modes);
}