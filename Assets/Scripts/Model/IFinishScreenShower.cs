﻿using System.Collections;

public interface IFinishScreenShower
{
    IEnumerator ShowFinishScreen(ResultHolder<FinishTypes> result);
}