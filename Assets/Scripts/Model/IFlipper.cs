﻿using System.Collections.Generic;

public interface IFlipper
{
    bool active { get; set; }
}