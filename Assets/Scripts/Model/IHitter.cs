﻿using System.Collections.Generic;

public interface IHitter
{
    IHitProcess Launch();
}