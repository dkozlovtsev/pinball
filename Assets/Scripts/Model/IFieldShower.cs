﻿public interface IFieldShower
{
    IDisposableField Show();
}