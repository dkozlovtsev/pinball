﻿using System;

public interface IAcceleration : IDisposable
{
    event Action accelerationStarted;
    event Action<float> accelerationChanged;
    event Action accelerationEnded;
}