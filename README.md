# Introduction
Simplest pinball for pc and mobile platforms with Gamefields editable through UnityEditorand Simple autoplay feature

# Running
## Editor
Load testScene scene and press play

## PC
Build for PC/Mac standalone
Run exe

## Android
Build for Android
Install apk on device
Run pinball app

#Unity version
5.4.4f1

#Tested on:
Windows 8.1
Nexus 5x Android 7.0

#Editing fields
NB!:The game uses unity physics to simulate ball movement, so all active objects have to have collider and (optionally) RigidBody attached
Any prefab with Field scripts on its root and added to Assets/Configs/Levels file is considered a gamefield
All public fields of Field scripts should be filled in order for field to work
To start try editing Default field prefab, move bumpers around
To create a new field from the ground up I recommend starting with empty field prefab, it have no bumper preinstalled
There are some bumper prefabs in Assets/Prefabs/Props, but any 3d model can be turned into bumper by making prefab out of it and adding collider, rigidbody(kinematic) and a Bumper script
Bumper script also defines how much point player gets for hitting this bumper
There is also an Accelerator script that can be attached to any trigger collider, it applies constant force to any non-kinematic object inside trigger
